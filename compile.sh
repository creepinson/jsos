#!/bin/bash
set -eo pipefail

echo_info() {
    echo -e "\033[1;32m[INFO]\033[0m $1"
}

# Variables
export ARCH=${ARCH:-x86_64}

echo_info "Building the rootfs..."

cd rootfs
mkdir dev || true
sudo mknod dev/ram b 1 0 || true
sudo mknod dev/console c 5 1 || true
find . | cpio -o -H newc | gzip >../initramfs.cpio.gz
cd ..

echo_info "Setting up the kernel sources..."
# if linux is not cloned, clone it
if [ ! -d "linux" ]; then
    git clone https://github.com/torvalds/linux --depth 1 --single-branch
fi

cd linux

echo_info "Creating the kernel config..."

make defconfig

# merge the config with ../kernel.config
if [ -f ../kernel.config ]; then
    echo_info "Merging the kernel config..."
    make oldconfig
    cat ../kernel.config >>.config
fi

echo_info "Building the linux kernel..."
make -j"$(nproc)"
cd ..

# gcc -static -o rootfs/init ../src/init.c

# grub-mkrescue -o timos.iso ./initrd

echo_info "Done!"
